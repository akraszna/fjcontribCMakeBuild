
# Find FastJet:
find_package( FastJet COMPONENTS fastjet fastjettools REQUIRED )

# Set up the build of the library:
fjcontrib_add_library( RecursiveTools
   PUBLIC_HEADERS Recluster.hh RecursiveSymmetryCutBase.hh
                  ModifiedMassDropTagger.hh SoftDrop.hh IteratedSoftDrop.hh
                  RecursiveSoftDrop.hh BottomUpSoftDrop.hh
   SOURCES        Recluster.cc RecursiveSymmetryCutBase.cc
                  ModifiedMassDropTagger.cc SoftDrop.cc IteratedSoftDrop.cc
                  RecursiveSoftDrop.cc BottomUpSoftDrop.cc
   LINK_LIBRARIES FastJet::fastjet FastJet::fastjettools )

# Set up the library's test(s):
foreach( test example_mmdt example_recluster example_softdrop
      example_recursive_softdrop example_advanced_usage example_isd
      example_bottomup_softdrop )

   fjcontrib_add_test( ${test}
      SOURCES        ${test}.cc
      INPUT_FILE     ${CMAKE_SOURCE_DIR}/data/single-event.dat
      REFERENCE      ${test}.ref
      LINK_LIBRARIES FastJet::fastjet FastJet::fastjettools RecursiveTools )

endforeach()

fjcontrib_add_test( example_mmdt_sub
   SOURCES        example_mmdt_sub.cc
   INPUT_FILE     ${CMAKE_SOURCE_DIR}/data/Pythia-Zp2jets-lhc-pileup-1ev.dat
   REFERENCE      example_mmdt_sub.ref
   LINK_LIBRARIES FastJet::fastjet FastJet::fastjettools RecursiveTools )

fjcontrib_add_test( example_mmdt_ee
   SOURCES        example_mmdt_ee.cc
   INPUT_FILE     ${CMAKE_SOURCE_DIR}/data/single-ee-event.dat
   REFERENCE      example_mmdt_ee.ref
   LINK_LIBRARIES FastJet::fastjet FastJet::fastjettools RecursiveTools )
