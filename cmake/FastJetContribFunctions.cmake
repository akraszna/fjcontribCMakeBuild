#
# Helper function(s) for building the FastJetContrib libraries.
#

# CMake include(s):
include( GNUInstallDirs )
include( CMakeParseArguments )

# Function building and installing a "standard" FastJetContrib library.
function( fjcontrib_add_library libName )

   # Read all the function arguments.
   set( _multiParamArgs PUBLIC_HEADERS SOURCES INCLUDE_DIRS LINK_LIBRARIES )
   cmake_parse_arguments( ARG "" "" "${_multiParamArgs}" ${ARGN} )
   unset( _multiParamArgs )

   # Set up the library target.
   add_library( ${libName} STATIC ${ARG_PUBLIC_HEADERS} ${ARG_SOURCES} )

   # Set up its properties.
   if( ARG_PUBLIC_HEADERS )
      set_property( TARGET ${libName}
         PROPERTY PUBLIC_HEADER ${PUBLIC_HEADERS} )
   endif()
   if( ARG_INCLUDE_DIRS )
      target_include_directories( ${libName} PUBLIC ${ARG_INCLUDE_DIRS} )
   endif()
   if( ARG_LINK_LIBRARIES )
      target_link_libraries( ${libName} PUBLIC ${ARG_LINK_LIBRARIES} )
   endif()

   # Set up its installation.
   install( TARGETS ${libName}
      ARCHIVE       DESTINATION "${CMAKE_INSTALL_LIBDIR}"
      PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/fastjet/contrib" )

endfunction( fjcontrib_add_library )

# Function building and running a test executable.
function( fjcontrib_add_test testName )

   # Read all the function arguments.
   set( _singleParamArgs INPUT_FILE REFERENCE )
   set( _multiParamArgs SOURCES INCLUDE_DIRS LINK_LIBRARIES )
   cmake_parse_arguments( ARG "" "${_singleParamArgs}" "${_multiParamArgs}"
      ${ARGN} )
   unset( _singleParamArgs )
   unset( _multiParamArgs )

   # Set up a unique target name for the executable:
   get_filename_component( _prefix ${CMAKE_CURRENT_SOURCE_DIR} NAME )
   set( _exeName "${_prefix}_${testName}" )

   # Set up the test executable.
   add_executable( ${_exeName} ${ARG_SOURCES} )

   # Set up its properties.
   set_property( TARGET ${_exeName} PROPERTY OUTPUT_NAME "${testName}" )
   if( ARG_INCLUDE_DIRS )
      target_include_directories( ${_exeName} PUBLIC ${ARG_INCLUDE_DIRS} )
   endif()
   if( ARG_LINK_LIBRARIES )
      target_link_libraries( ${_exeName} PUBLIC ${ARG_LINK_LIBRARIES} )
   endif()

   # Set up a run directory for the test:
   set( _runDir "${CMAKE_CURRENT_BINARY_DIR}/testRun/${testName}" )
   file( MAKE_DIRECTORY "${_runDir}" )

   # Set up the test, using the executable.
   add_test( NAME "${_exeName}"
      COMMAND ${CMAKE_SOURCE_DIR}/cmake/check.sh
      ${CMAKE_CURRENT_BINARY_DIR}/${testName} ${ARG_INPUT_FILE}
      ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_REFERENCE}
      WORKING_DIRECTORY "${_runDir}" )

   # Set up its properties.
   set_tests_properties( "${_exeName}" PROPERTIES
      LABELS "${_prefix}" )

endfunction( fjcontrib_add_test )
