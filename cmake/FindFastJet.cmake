# Helper module for finding an installed FastJet release, to be used as the
# basis for the fjcontrib build.
#
# It defines the following "simple" variables:
#  - FastJet_FOUND: An uncached boolean flag showing whether FastJet was found.
#  - FastJet_INCLUDE_DIR: The cached include directory for the FastJet headers.
#  - FastJet_INCLUDE_DIRS: The uncached (list of) include directory/directories
#    of FastJet headers.
#  - FastJet_LIBRARIES: The uncached FastJet libraries to link against.
#  - FastJet_LIBRARY_DIRS: The uncached (list of) library directory/directories
#    to use during the linking.
#  - FastJet_<COMPONENT>_LIBRARY: The cached name of a given FastJet library
#    file.
#  - FastJet_VERSION: The uncached version of the found FastJet code.
#

# Silence the module if FastJet was already found:
if( FastJet_FOUND )
   set( FastJet_FIND_QUIETLY TRUE )
endif()

# Look for the FastJet headers:
find_path( FastJet_INCLUDE_DIR
   NAMES fastjet/version.hh fastjet/config.h
   PATH_SUFFIXES include
   HINTS ${FastJet_ROOT} $ENV{FastJet_ROOT} )
set( FastJet_INCLUDE_DIRS ${FastJet_INCLUDE_DIR} )

# Determine the version of FastJet that was found:
if( FastJet_INCLUDE_DIR AND
      EXISTS "${FastJet_INCLUDE_DIR}/fastjet/config_auto.h" )
   file( STRINGS "${FastJet_INCLUDE_DIR}/fastjet/config_auto.h" _version
      REGEX "define *FASTJET_VERSION " )
   string( REGEX MATCH "\"([0-9\\.]+)\"" _version ${_version} )
   set( FastJet_VERSION ${CMAKE_MATCH_1} )
   unset( _version )
endif()

# Find all of the FastJet libraries requested. Note that the main "fastjet"
# library is always included.
set( FastJet_LIBRARIES )
set( FastJet_LIBRARY_DIRS )
foreach( _library fastjet ${FastJet_FIND_COMPONENTS} )
   # Search for the library:
   find_library( FastJet_${_library}_LIBRARY
      NAMES ${_library}
      PATH_SUFFIXES lib lib32 lib64
      HINTS ${FastJet_ROOT} $ENV{FastJet_ROOT} )
   mark_as_advanced( FastJet_${_library}_LIBRARY )
   # If found, add it to the libraries list, and add its directory to the
   # library directory list.
   if( FastJet_${_library}_LIBRARY )
      list( APPEND FastJet_LIBRARIES ${FastJet_${_library}_LIBRARY} )
      get_filename_component( _libdir ${FastJet_${_library}_LIBRARY}
         DIRECTORY )
      list( APPEND FastJet_LIBRARY_DIRS ${_libdir} )
      unset( _libdir )
      # Also, if one doesn't exist for it yet, set up an imported target for
      # the library.
      if( NOT TARGET FastJet::${_library} )
         add_library( FastJet::${_library} SHARED IMPORTED )
         set_target_properties( FastJet::${_library} PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${FastJet_INCLUDE_DIRS}"
            IMPORTED_LOCATION "${FastJet_${_library}_LIBRARY}" )
      endif()
   endif()
endforeach()

# Remove duplicates from the lists:
if( FastJet_LIBRARIES )
   list( REMOVE_DUPLICATES FastJet_LIBRARIES )
endif()
if( FastJet_LIBRARY_DIRS )
   list( REMOVE_DUPLICATES FastJet_LIBRARY_DIRS )
endif()

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( FastJet
   FOUND_VAR FastJet_FOUND
   REQUIRED_VARS FastJet_INCLUDE_DIR FastJet_LIBRARIES FastJet_LIBRARY_DIRS
   VERSION_VAR FastJet_VERSION )
mark_as_advanced( FastJet_FOUND FastJet_INCLUDE_DIR )

# Remember the FastJet_FOUND variable so that only the first call to this
# module would show up in the terminal.
set( FastJet_FOUND ${FastJet_FOUND}
   CACHE BOOL "The found state of FastJet" )
